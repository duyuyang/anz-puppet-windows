# Class: bambooagent
#
# This module manages bambooagent
#
# Parameters: none
#
# Actions:
#
# Requires: see Modulefile
#
# Sample Usage:
#
class bambooagent {
  include package
}
