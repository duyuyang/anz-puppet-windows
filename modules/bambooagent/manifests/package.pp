#== Class: bambooagent::package
#
# This module installs bamboo agent
#
# Run the bamboo agent after installation
#
class bambooagent::package{
  # Download Bamboo Agent from URL
  $bambooserver = 'sqlau801mel0075.globaltest.anz.com'
  $bambooagentjar = 'atlassian-bamboo-agent-installer-5.7.1.jar'
  $install_url = "http://${bambooserver}:8085/agentServer/agentInstaller/${bambooagentjar}"
  $remoteagenthome = 'D:\\BambooAgent'
  $jre7exe = '"C:\Program Files\Java\jre7\bin\java.exe"'

  file {$remoteagenthome:
    ensure => directory,
  }  

  # Download the jar file from Bamboo Server
  staging::file {'bambooagentjardownload':
    source  => $install_url,
    target  => "${remoteagenthome}/${bambooagentjar}",
    curl_option => "--noproxy ${bambooserver}",
    require => File[$remoteagenthome],
  } 
 
  # Deploy Bamboo Agent Jar
  exec {'runbambooagent':
    command  => "${jre7exe} -Dbamboo.home=${remoteagenthome} -jar ${remoteagenthome}\\${bambooagentjar} http://${bambooserver}:8085/agentServer installntservice",
    creates  => "${remoteagenthome}\\bin\\BambooAgent.bat",
    provider => windows,  
    require  => [
    	Package['javaruntime'],
        Staging::File['bambooagentjardownload'],
    ],
  }

  # Bamboo Agent Recovery settings
  registry::value { 'FailureActions':
    key   => 'HKLM\SYSTEM\CurrentControlSet\services\bamboo-remote-agent\FailureActions',
    type => binary,
    data  => '00 00 00 00 00 00 00 00 00 00 00 00 03 00 00 00 14 00 00 00 01 00 00 00 60 EA 00 00 01 00 00 00 60 EA 00 00 01 00 00 00 60 EA 00 00',
    require => Exec['runbambooagent'],
  }

  # Start the agent / Restart the agent
   service {'bamboo-remote-agent':
     ensure => 'running',
     require => Exec['runbambooagent'],
   }
}
