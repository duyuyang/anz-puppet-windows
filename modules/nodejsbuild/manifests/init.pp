# Class: nodejsbuild
#
# This module manages nodejsbuild
#
# Parameters: none
#
# Actions:
#
# Requires: see Modulefile
#
# Sample Usage:
#
class nodejsbuild {
  include nodejs
  include npm
}
