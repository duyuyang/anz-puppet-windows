#== Class nodejsbuild::npm
#
# This module installs npm via chocolatey
#
#
class nodejsbuild::npm{
  include windows_path
  include stdlib
  
  $systemhome = "C:\\WINDOWS\\system32\\config\\systemprofile"
  $npmhome = "C:\\WINDOWS\\system32\\config\\systemprofile\\AppData\\Roaming\\npm"
  $nodehome = "C:\\Progra~1\\nodejs"
  $npmbin = "C:\\Program Files\\nodejs\\node_modules\\.bin"

  file{$npmhome:
    ensure => directory,
  }

  windows_path { 'npmbin':
	ensure    => present,
	directory => $npmbin,
  }
 
  file { 'npmrc':
	ensure  => present,
	path    => "$systemhome/.npmrc",
	content => template('nodejsbuild/npmrc.erb'),
  }

  file { 'bowerrc':
	ensure  => present,
	path    => "$systemhome/.bowerrc",
	content => template('nodejsbuild/bowerrc.erb'),
  }
     
  exec { 'npm-http-proxy':
    command  => "npm.cmd config set proxy http://127.0.0.1:3128",
    path     => $nodehome,
    provider => 'powershell',
    require  => Package['nodejs.install'],
  }

  exec { 'npm-https-proxy':
    command  => "npm.cmd config set https-proxy http://127.0.0.1:3128",
    path     => $nodehome,
    provider => 'powershell',
    require  => Package['nodejs.install'],
  }
  
  exec { 'npm-ssl':
    command  => "npm.cmd config set strict-ssl false",
    path     => $nodehome,
    provider => 'powershell',
    require  => Package['nodejs.install'],
  }

  exec { 'npm-registry':
    command  => "npm.cmd config set registry http://registry.npmjs.org",
    provider => 'powershell',
    path     => $nodehome,
    require  => Package['nodejs.install'],
  }
  
  exec { 'npm-install-bower':
	creates  => 'C:\Program Files\nodejs\node_modules\bower\bin\bower',
    command  => "npm.cmd install bower",
    provider => 'powershell',
    path     => $nodehome,  
	cwd		 => $nodehome,
	require  => Package['nodejs.install'],
  }
  
  exec { 'npm-install-osenv':
	creates  => 'C:\Program Files\nodejs\node_modules\osenv\osenv.js',
    command  => "npm.cmd install osenv",
    provider => 'powershell',
    path     => $nodehome,  
	cwd		 => $nodehome,
	require  => Package['nodejs.install'],
  }
  
  exec { 'npm-install-grunt-cli':
	creates  => 'C:\Program Files\nodejs\node_modules\grunt-cli\bin\grunt',
    command  => "npm.cmd install grunt-cli",
    provider => 'powershell',
    path     => $nodehome,
	cwd		 => $nodehome,	
	require  => Package['nodejs.install'],
  }

  exec { 'npm-install-cucumber':
	creates  => 'C:\Program Files\nodejs\node_modules\cucumber\bin\cucumber.js',
    command  => "npm.cmd install cucumber",
    provider => 'powershell',
    path     => $nodehome,
	cwd		 => $nodehome,	
	require  => Package['nodejs.install'],
  }

  exec { 'npm-install-http-server':
	creates  => 'C:\Program Files\nodejs\node_modules\http-server\bin\http-server',
    command  => "npm.cmd install http-server",
    provider => 'powershell',
    path     => $nodehome,  
	cwd		 => $nodehome,	
	require  => Package['nodejs.install'],
  }

  file { 'bower':
	ensure  => present,
	path    => "$nodehome/node_modules/bower/bin/bower",
	content => template('nodejsbuild/bower.erb'),
	require => Exec['npm-install-bower'],
  }
  
  file { 'grunt':
	ensure  => present,
	path    => "$nodehome/node_modules/grunt-cli/bin/grunt",
	content => template('nodejsbuild/grunt.erb'),
	require => Exec['npm-install-grunt-cli'],
  }
 
  file { 'http-server':
	ensure  => present,
	path    => "$nodehome/node_modules/http-server/bin/http-server",
	content => template('nodejsbuild/http-server.erb'),
	require => Exec['npm-install-http-server'],
  }

}
