#== Class: nodejsbuild::nodejs
#
# This module installs nodejs via chocolatey
#
class nodejsbuild::nodejs{
  
  # Install package nodejs
  package{ 'nodejs.install':
    ensure   => installed,
    provider => chocolatey,
    require  => Exec['install-chocolatey'],
  }
}
