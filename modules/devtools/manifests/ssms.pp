#== Class: devtools::ssms
#
# Install SQL Server Management Studio
#
# Also installs Microsoft Visual C++ 2010
#
#
class devtools::ssms{
  
  # SQL Server Requires .Net Framework 3.5
  exec {'installdotnet':
    creates => 'C:\Windows\Microsoft.NET\Framework64\v3.5\csc.exe',
    command => template('devtools/dotnet35.ps1'),
    provider => powershell,
  }

  # Install SQL Server Management Studio from source
  exec {'installssms':
    creates  => 'C:\Program Files (x86)\Microsoft SQL Server\110\Tools\Binn\ManagementStudio\Ssms.exe',
    command  => "${common::source}\\SQLEXPRWT_x64_ENU.exe /QUIETSIMPLE=false /QUIET=true /INDICATEPROGRESS=false /Action=INSTALL /IACCEPTSQLSERVERLICENSETERMS /FEATURES=SSMS,LocalDB /UPDATEENABLED=false /ERRORREPORTING=false /SQMREPORTING=0 /SKIPRULES='HasSecurityBackupAndDebugPrivilegesCheck'",
    provider =>  powershell,
    require  => Exec['installdotnet'],
  }

}
