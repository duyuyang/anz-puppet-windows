#== Class: devtools::sourcetree
#
# This module installs SourceTree with lastest version
# This module has no dependencies
# https://chocolatey.org/packages/SourceTree
#
#
class devtools::sourcetree{

  # Install SourceTree
  package {'SourceTree':
    ensure          =>  installed,
    provider        =>  chocolatey,
    require         =>  Exec['install-chocolatey'],
  }

}
