# Class: devtools
#
# This module manages devtools
#
# Parameters: none
#
# Actions:
#
# Requires: see Modulefile
#
# Sample Usage:
#
class devtools {

  include editor
  include sourcetree
  include ssms
  
  if $common::source == undef {
    fail "Common::Source needs to be set"
  }
  


}
