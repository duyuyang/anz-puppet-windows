#== Class: devtools:editor
#
# This module installs devtools
#
class devtools::editor{
  
    # Install sublime
    package { 'SublimeText3':
      ensure          => installed,
      provider        => chocolatey,
      require         => Exec['install-chocolatey'],
    }   
    
  # Install JetBrains IntelliJ IDEA - Ultimate Edition 13.1.5
    package {'intellijidea-ultimate':
      ensure          =>  installed,
      provider        =>  chocolatey,
      require         =>  Exec['install-chocolatey'],
    }
}
