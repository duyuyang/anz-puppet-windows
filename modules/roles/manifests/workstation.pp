# == Class: roles
#
# Full description of class roles here.
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.
class roles::workstation {

	anchor { '::roles::workstation::start': } ->
		class { '::common': } ->
		class { '::javabuild': } ->
		class { '::rubybuild': } ->
		class { '::nodejsbuild': } ->
		class { '::devtools': } ->
	anchor { '::roles::workstation::end': }
}

include roles::workstation
