# == Class: rubybuild
#
# Installs and configures Ruby 2.1 to get through the ANZ proxy.
# Assumes that the CNTLM proxy has already been configured
#
# === Authors
#
# Author Name <timothy.mukaibo@odecee.com.au>
#
# === Copyright
#
# Copyright 2014 Timothy Mukaibo
#
class rubybuild {

	$ruby_bin = 'C:\tools\ruby213\bin'
	$bundler_ver = '1.7.5'
	$proxy = 'http://127.0.0.1:3128'
	$gempath = 'C:\tools\ruby213\lib\ruby\gems\2.1.0\gems'
	
	include package
	include gems
}
