class rubybuild::package {
	include windows_path
	include stdlib

	package { 'ruby':
		ensure   => '2.1.3.0',
		provider => 'chocolatey',
	}

	package { 'ruby2.devkit':
		ensure   => present,
		require  => Package['powershell4'],
		provider => 'chocolatey',
	}

	windows_path { 'devkit_bin':
		ensure    => present,
		directory => 'C:\tools\DevKit2\bin',
	}

	windows_path { 'devkit_mingw_bin':
		ensure    => present,
		directory => 'C:\tools\DevKit2\mingw\bin',
	}

	file { 'rake-bat':
		ensure  => present,
		require => Package['ruby'],
		path    => "${rubybuild::ruby_bin}/rake.bat",
		content => template('rubybuild/rake/rake.bat.erb'),
	}
}
