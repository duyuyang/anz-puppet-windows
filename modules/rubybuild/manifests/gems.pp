class rubybuild::gems {
    $systemhome = "C:\\WINDOWS\\system32\\config\\systemprofile"	
	define gem($gemname = $title, $version, $options='') {
		exec { "gem-${gemname}-${version}":
			require  => Package['ruby'],
			command  => "C:/tools/ruby213/bin/gem.bat install ${gemname} -v '${version}' --source 'http://rubygems.org' ${options}",
			provider => 'powershell',
			path     => 'C:\tools\ruby213\bin;C:\tools\DevKit2\mingw\bin;C:\tools\DevKit2\bin',
			onlyif   => "!(Test-Path ${rubybuild::gempath}\\${gemname}-${version})",
			timeout  => 0,
		}

	}

	gem { 'bundler':
		version => $rubybuild::bundler_ver
	}

	gem { 'hitimes':
		version => '1.2.2',
		options => '--platform=ruby',
	}

	gem {'foundation':
		version => '1.0.4',
		options => '--platform=ruby',
	}
	
	gem {'compass':
		version => '1.0.3',
		options => '--platform=ruby',
	}	

	gem {'jruby-rack':
		version => '1.1.18',
		options => '--platform=ruby',
	}	

	gem {'warbler':
		version => '1.4.5',
		options => '--platform=ruby',
	}
	
	# Need to specify platform=ruby or else the Lexer won't work:
	# https://github.com/cucumber/gherkin/issues/320
	gem { 'gherkin':
		version => '2.12.2',
		options => '--platform=ruby',
	}

	file { 'gherkin-monkey-patch':
		ensure  => present,
		require => Gem['gherkin'],
		path    => "${rubybuild::gempath}/gherkin-2.12.2/lib/gherkin/c_lexer.rb",
		content => template('rubybuild/gherkin/c_lexer.rb'),
	}

	file { 'gemrc':
		ensure  => present,
		path    => "$systemhome/.gemrc",
		content => template('rubybuild/gemrc.erb'),
	}	
	
	file { 'bundler-bat':
		ensure  => present,
		require => Gem['bundler'],
		path    => "${rubybuild::ruby_bin}/bundle.bat",
		content => template('rubybuild/bundler/bundle.bat.erb'),
	}

	file { 'bundler-monkey-patch':
		ensure  => present,
		require => Gem['bundler'],
		path    => "${rubybuild::gempath}/bundler-${rubybuild::bundler_ver}/lib/bundler/fetcher.rb",
		content => template('rubybuild/bundler/fetcher.rb.erb'),
	}
}
