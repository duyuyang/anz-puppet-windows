class wintomcat::service {
	service { 'Tomcat7':
		ensure  => running,
		enable  => true,
		require => Exec['install-tomcat'],
	}
}
