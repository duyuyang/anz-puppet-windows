class wintomcat::config {
	file { 'C:/Program Files/Apache Software Foundation/Tomcat 7.0/conf/tomcat-users.xml':
		content => template('wintomcat/tomcat-users.xml'),
		require => Exec['install-tomcat'],
		notify  => Service['Tomcat7'],
	}

	file { 'C:/Program Files/Apache Software Foundation/Tomcat 7.0/conf/context.xml':
		content => template('wintomcat/context.xml'),
		require => Exec['install-tomcat'],
		notify  => Service['Tomcat7'],
	}
	
	# Configure the Tomcat memory. Failure to do this stops the WAR file from running.
	registry::value { 'tomcat-memory':
		key     => 'HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Apache Software Foundation\Procrun 2.0\Tomcat7\Parameters\Java',
		value   => 'options',
		data    => [
			'-Dcatalina.home=C:\Program Files\Apache Software Foundation\Tomcat 7.0',
			'-Dcatalina.base=C:\Program Files\Apache Software Foundation\Tomcat 7.0',
			'-Djava.endorsed.dirs=C:\Program Files\Apache Software Foundation\Tomcat 7.0\endorsed',
			'-Djava.io.tmpdir=C:\Program Files\Apache Software Foundation\Tomcat 7.0\temp',
			'-Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager',
			'-Djava.util.logging.config.file=C:\Program Files\Apache Software Foundation\Tomcat 7.0\conf\logging.properties',
			'-DUSERNAME_CSP=ci',
			'-XX:MaxPermSize=512M',
		],
		type    => 'array',
		notify  => Service['Tomcat7'],
		require => Exec['install-tomcat'],
	}
	
	registry::value { 'jvm-mem-min':
		key     => 'HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Apache Software Foundation\Procrun 2.0\Tomcat7\Parameters\Java',
		value   => 'JvmMs',
		type    => 'dword',
		data    => '2048', # 2GB
		notify  => Service['Tomcat7'],
		require => Exec['install-tomcat'],
	}
	
	registry::value { 'jvm-mem-max':
		key     => 'HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Apache Software Foundation\Procrun 2.0\Tomcat7\Parameters\Java',
		value   => 'JvmMx',
		type    => 'dword',
		data    => '4096', # actually 4GB
		notify  => Service['Tomcat7'],
		require => Exec['install-tomcat'],
	}
		
}
