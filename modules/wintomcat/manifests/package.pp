class wintomcat::package {
	$tomcat_installer = 'apache-tomcat-7.0.57.exe'
	$installer_url = "http://mirror.rackcentral.com.au/apache/tomcat/tomcat-7/v7.0.57/bin/${tomcat_installer}"
	$download_base = 'C:/tools/files'
	$jtds_zip = "${download_base}/jtds-1.2.8.zip"
	$jtds_path = "${download_base}/jtds-1.2.8-dist"
	$driver = "${jtds_path}/jtds-1.2.8.jar"
	
	file { $download_base:
		ensure => directory,
	}

        file {'C:/tools':
		ensure => directory,
        }

	staging::file { 'tomcat-installer':
		source      => $installer_url,
		target      => "${download_base}/${tomcat_installer}",
		require     => Registry::Value['use-system-proxy'],
		curl_option => '--proxy http://127.0.0.1:3128',
	}

	exec { 'install-tomcat':
		command     => "${download_base}/${tomcat_installer} /S",
		creates     => 'C:/Program Files/Apache Software Foundation/Tomcat 7.0',
		provider    => 'windows',
		environment => [ 
			'JAVAHOME="C:\Program Files\Java\jdk1.6.0_45"', 
			'PATH="C:\Program Files\Java\jdk1.6.0_45\bin"',
		],
		require     => [
			Staging::File['tomcat-installer'],
                        Package['javaruntime'],
                ],
		logoutput   => true,
	}

	# JTDS - this is the SQL Server driver for Java.
	# The last version of JTDS to support Java 1.6 is the 1.2.x branch. 1.3+ requires Java 7.
	staging::file { 'jtds-1.2':
		source  => "http://downloads.sourceforge.net/project/jtds/jtds/1.2.8/jtds-1.2.8-dist.zip?r=http%3A%2F%2Fsourceforge.net%2Fprojects%2Fjtds%2Ffiles%2Fjtds%2F1.2.8%2F&ts=1416203703&use_mirror=liquidtelecom",
		target  => $jtds_zip,
		require => Exec['install-tomcat'],
	}

	file { $jtds_path:
		ensure => directory,
	}

	windows::unzip { $jtds_zip:
		destination => $jtds_path,
		creates     => $driver,
		require     => [ Staging::File['jtds-1.2'], File[$jtds_path] ],
	}

	file { 'C:/Program Files/Apache Software Foundation/Tomcat 7.0/lib/jtds-1.2.8.jar':
		ensure             => present,
		source             => $driver,
		source_permissions => 'ignore',
		require            => Windows::Unzip[$jtds_zip],
	}
}
