# == Class: tomcat
#
# Installs and configures Tomcat 7 on Windows
#
# === Authors
#
# Timothy Mukaibo <timothy.mukaibo@odecee.com.au>
#
# === Copyright
#
# Copyright 2014 Timothy Mukaibo
#
class wintomcat {

	if( ! defined(Package['javaruntime'])) {
		fail 'A Java 7 Runtime needs to be managed by Puppet for this module to work.'
	}

	include package
	include config
	include service
}
