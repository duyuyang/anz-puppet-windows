#== Class: javabuild::gradle
# 
# This module installs gradle from gradle-2.2-all.zip
#
class javabuild::gradle{
  # Install Gradle 2.2 from source
  # Problem: How to make sure JAVA_HOME and PATH has been set before installing gradle?
  # Deploying Gradle
  # https://forge.puppetlabs.com/counsyl/windows#windowsunzip

  windows::unzip {"${common::source}\\gradle-2.2-all.zip":  
    destination => 'C:\Program Files',
    creates     => 'C:\Program Files\gradle-2.2\bin\gradle.bat',
    require     => Package['javajdk'],
  }

  # Add Gradle directory to path environment
  windows_path {'addgradletopath':
    ensure      => present,
    directory   => 'C:\Program Files\gradle-2.2\bin',
  } 
}