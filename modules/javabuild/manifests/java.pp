# == Class: javabuild::jdk
#
# Install jdk1.6 from the source directory
#
#
#
class javabuild::java{
  # Install Java 6 JDK from source
  # http://stackoverflow.com/questions/21275711/how-to-install-uninstall-windows-service-using-puppet
  package { 'javajdk':
    ensure          => installed,
    provider        => windows,
    source          => "${common::source}\\jdk-6u45-windows-x64.exe",
    install_options => ['/quiet'],
  }
  
  #Install Java Run Time for Bamboo Agent
  package{'javaruntime':
      ensure          => installed,
      provider        => chocolatey,    
      require         => Exec['install-chocolatey'],       
  }  

  # Modify JAVA_HOME 
  windows::environment { 'JAVA_HOME':
     value    => 'C:\Program Files\Java\jdk1.6.0_45',
     require  => [Package['javajdk'],
                  Package['javaruntime'],
                 ],
  } 
  
  # Modify system PATH
  windows_path {'addjavatopath':
    ensure      => present,
    directory   => 'C:\Program Files\Java\jdk1.6.0_45\bin',
  }   
  
}
