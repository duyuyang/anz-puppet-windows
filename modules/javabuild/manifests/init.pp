# Class: javabuild
#
# This module manages javabuild
#
# Parameters: none
#
# Actions:
#
# Requires: see Modulefile
#
# Sample Usage:
#
class javabuild {
   #$source = "//VBOXSVR/share"
  include gradle
  include java
  
  if $common::source == undef {
    fail "Common::Source needs to be set"
  }


}
