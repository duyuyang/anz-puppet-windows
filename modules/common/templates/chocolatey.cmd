@echo off

SET http_proxy="http://127.0.0.1:3128"
SET https_proxy="http://127.0.0.1:3128"

SET DIR=%~dp0%

if '%1'=='/?' goto usage
if '%1'=='-?' goto usage
if '%1'=='?' goto usage
if '%1'=='/help' goto usage
if '%1'=='help' goto usage
if '%1'=='--help' goto usage

SET PS_ARGS=%*
IF NOT '%1'=='' SET PS_ARGS=%PS_ARGS:"=\"%
IF NOT '%1'=='' SET PS_ARGS=%PS_ARGS:\\"=\"%

@PowerShell -NoProfile -NoLogo -ExecutionPolicy unrestricted -Command "$env:https_proxy='http://127.0.0.1:3128'; $env:http_proxy='http://127.0.0.1:3128';[System.Threading.Thread]::CurrentThread.CurrentCulture = ''; [System.Threading.Thread]::CurrentThread.CurrentUICulture = '';& '%DIR%chocolatey.ps1' %PS_ARGS%"
SET ErrLvl=%ERRORLEVEL%
goto :exit

:exit
exit /b %ErrLvl%

goto :eof
:usage

@PowerShell -NoProfile -NoLogo -ExecutionPolicy unrestricted -Command "$env:https_proxy='http://127.0.0.1:3128'; $env:http_proxy='http://127.0.0.1:3128';[System.Threading.Thread]::CurrentThread.CurrentCulture = ''; [System.Threading.Thread]::CurrentThread.CurrentUICulture = '';& '%DIR%chocolatey.ps1' help"
