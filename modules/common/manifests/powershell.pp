class common::powershell {
	package { 'powershell4':
		ensure   => installed,
		provider => 'chocolatey',
		require  => Exec['install-chocolatey'],
	}
}
