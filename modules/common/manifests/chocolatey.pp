class common::chocolatey {

	# Install chocolately. Note that the Proxy needs to be configured first,
	# see the dependency chaining in init.pp.
	exec { 'install-chocolatey':
		creates  => 'C:\ProgramData\chocolatey\bin\choco.exe',
		require  => [  
			File['C:\ProgramData\chocolatey\chocolateyinstall\helpers\functions\Get-WebFile.ps1'],
			File['C:\ProgramData\chocolatey\chocolateyinstall\helpers\functions\Get-WebHeaders.ps1'],
			Exec['nuget-proxy'],
		],
		provider => 'powershell',
		command  => '$true',
	}

	exec { 'download-chocolatey':
		creates  => 'C:\ProgramData\chocolatey\bin\choco.exe',
		provider => 'powershell',
		command  => template('common/chocoproxy.ps1.erb'),
		require  => [ Exec['root-ca'], Registry::Value['use-system-proxy'] ],
	}

	file { 'C:\ProgramData\chocolatey\chocolateyinstall\helpers\functions\Get-WebFile.ps1':
		ensure  => file,
		content => template('common/Get-WebFile.ps1'),
		require => Exec['download-chocolatey'],
	}

	file { 'C:\ProgramData\chocolatey\chocolateyinstall\helpers\functions\Get-WebHeaders.ps1':
		ensure  => file,
		content => template('common/Get-WebHeaders.ps1'),
		require => Exec['download-chocolatey'],
	}
	
	exec { 'nuget-proxy':
		command  => 'C:\ProgramData\chocolatey\chocolateyinstall\nuget.exe config -Set HTTP_PROXY=http://127.0.0.1:3128',
 		provider => 'powershell',
		require  => Exec['download-chocolatey'],
	}
}
