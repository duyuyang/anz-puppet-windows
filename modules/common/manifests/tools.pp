#== Class: common:tools
#
# This module installs common tools
#
class common::tools{
  
    # Install VIM, Need a condition for existing of chocolatey
    package { 'vim':
      ensure          => installed,
      provider        => chocolatey,     
      require         => Exec['install-chocolatey'],
    }

    # Install notepad++
    package { 'notepadplusplus':
      ensure          => installed,
      provider        => chocolatey,
      require         => Exec['install-chocolatey'],
    }

	# Install google chrome
    package { 'google-chrome-x64':
      ensure          => installed,
      provider        => chocolatey,
      require         => Exec['install-chocolatey'],
    }

}
