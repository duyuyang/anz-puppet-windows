class common::puppet {

	$provisioning_root = "C:/ProgramData/Provisioning.git" 
	$puppet_modules = "${provisioning_root}/modules"

	if($::operatingsystemrelease == '2008 R2') {
		$buildtype = 'buildagent'
	} else {
		$buildtype = 'workstation'
	}


	# Ensure Git is in the path. This allows the use of Curl, which is necessary to download some stuff with Staging
	windows_path {'gitpath':
		ensure      => present,
		directory   => 'C:\Program Files (x86)\Git\bin',
	} 

	# Masterless Puppet is a two-step process. First we need to pull the latest modules from the repo.
	# Then we need to trigger a Puppet run. We use two scheduled tasks to do this, but another option would
	# be to merge into a single Powershell script...
	scheduled_task { 'pull-modules':
		ensure      => 'present',
		enabled     => true,
		require     => Windows_path['gitpath'],
		command     => 'c:/windows/system32/cmd.exe',
		arguments   => '/c set https_proxy="http://127.0.0.1:3128" && "C:/Program Files (x86)/Git/bin/git.exe" pull',
		working_dir => $provisioning_root,
		trigger     => {
			schedule   => 'daily',
			start_time => '12:30',
		}
	}

	scheduled_task { 'puppet-apply':
		ensure      => 'present',
		enabled     => true,
		require     => Windows_path['gitpath'],
		command     => 'C:/Program Files/Puppet Labs/Puppet/bin/puppet.bat',
		arguments   => "apply ${puppet_modules}/roles/manifests/${buildtype}.pp --modulepath ${puppet_modules}",
		working_dir => $provisioning_root,
		trigger     => {
			schedule   => 'daily',
			start_time => '12:45',
		}
	}
}
