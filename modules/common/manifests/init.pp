# Class: common
#
# This module configures some common stuff that is shared by multiple modules
# and roles.
#
class common {
	# Many other modules use this so they don't need to go out to the Internet.
	$source = '\\sqlau801mel0075.globaltest.anz.com\data\provisioning\files'

	include common::certificate
	include common::proxy
	include common::chocolatey
	include common::puppet
	include common::powershell
	include common::tools

	Class['Common::Certificate'] -> Class['Common::Proxy'] -> Class['Common::Chocolatey'] -> Class['Common::Powershell'] -> Class['Common::Puppet']
}
