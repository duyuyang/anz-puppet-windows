class common::certificate {
	# Not sure how to prevent this from running each time...
	exec { 'root-ca':
		command  => "certutil.exe -addstore -f Root ${common::source}\\root_ca.cer",
		path     => 'C:/windows/system32',
		provider => 'windows',
		require  => Exec['ca-chain'],
	}

	exec { 'ca-chain':
		command  => "certutil.exe -addstore -f Root ${common::source}\\chain.cer",
		path     => 'C:/windows/system32',
		provider => 'windows',
	}
}
