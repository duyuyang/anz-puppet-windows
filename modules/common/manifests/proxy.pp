# Configures the proxy for Internet Explorer (both 32 and 64-bit)
class common::proxy {
	registry::value { 'use-system-proxy':
		key     => 'HKLM\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings',
		value   => 'ProxySettingsPerUser',
		type    => 'dword',
		data    => 0,
		require => [ 
			Registry::Value['enable-proxy'],
			Registry::Value['proxy-server'],
			Registry::Value['noproxy-hosts'],
			Registry::Value['auto-proxy-config'],
			Registry::Value['enable-proxy-32'],
			Registry::Value['proxy-server-32'],
			Registry::Value['noproxy-hosts-32'],
			Registry::Value['auto-proxy-config-32'],
			Registry::Value['disable-ie-esc-admins'],
			Registry::Value['disable-ie-esc-users'],
			Windows::Environment['http_proxy'],
			Windows::Environment['https_proxy'],
			Windows::Environment['no_proxy'],
			File['C:/Program Files (x86)/Git/etc/gitconfig'],
		]
	}

	windows::environment { 'https_proxy':
		value => '127.0.0.1:3128',
	}

	windows::environment { 'http_proxy':
		value => '127.0.0.1:3128',
	}

	windows::environment { 'no_proxy':
		value => 'localhost,.globaltest.anz.com',
	}

	# Disable SSL Verification - required for SYSTEM to bundle install from git
	file { 'C:/Program Files (x86)/Git/etc/gitconfig':
		ensure  => present,
		content => template('common/gitconfig'),
	}

	# Internet Explorer Enhanced Security - basically makes IE unusable. We want to disable this so we can access the internet.
	registry::value { 'disable-ie-esc-admins':
		key   => 'HKLM\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A7-37EF-4b3f-8CFC-4F3A74704073}',
		value => 'IsInstalled',
		data  => 0,
		type  => 'dword',
	}

	registry::value { 'disable-ie-esc-users':
		key   => 'HKLM\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A8-37EF-4b3f-8CFC-4F3A74704073}',
		value => 'IsInstalled',
		data  => 0,
		type  => 'dword',
	}

	registry::value { 'enable-proxy-32':
		key     => 'HKLM\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Internet Settings',
		value   => 'ProxyEnable',
		data    => 1,
		type    => 'dword',
	}

	registry::value { 'proxy-server-32':
		key     => 'HKLM\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Internet Settings',
		value   => 'ProxyServer',
		data    => '127.0.0.1:3128',
		type    => 'string',
	}
 
	registry::value { 'noproxy-hosts-32':
		key     => 'HKLM\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Internet Settings',
		value   => 'ProxyOverride',
		data    => 'wasau801mel0004.globaltest.anz.com;csptraining.service.dev;dbdtw01l.unix.anz;dbdtw02l.unix.anz;dccrowd.service.dev;dcjira.service.dev;dcartifactory.service.dev;dcbamboo.service.dev;dcstash.service.dev;dcconfluence.service.dev;mydesk.dev.anz;mypasswordreset.apps.anz;sqlau801mel0077.globaltest.anz.com;cspqa.service.dev;cspdev01.service.dev;sqlau801mel0092.globaltest.anz.com;wasau301mel0088.globaltest.anz.com;sharepoint.apps.anz;fiatintegration.service.dev;csptst.service.dev;sqlau801mel0080.globaltest.anz.com;confluence.service.anz;wasau601mel0003.globaltest.anz.com;wasau601mel0002.globaltest.anz.com;cspsit.service.dev;sqlau801mel0093.globaltest.anz.com;sqlau801mel0075.globaltest.anz.com;sqlau801mel0076.globaltest.anz.com;sqlau801mel0036.globaltest.anz.com;confluence.service.anz;cspdev.service.dev;serveradmin.apps.anz;.corp.anz.com;localhost;wasau301mel0019.globaltest.anz.com;<local>',
		type    => 'string',
	}

	registry::value { 'auto-proxy-config-32':
		key   => 'HKLM\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Internet Settings',
		value => 'AutoDetect',
		data  => 0,
		type  => 'dword',
	}

	registry::value { 'enable-proxy':
		key     => 'HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings',
		value   => 'ProxyEnable',
		data    => 1,
		type    => 'dword',
	}

	registry::value { 'proxy-server':
		key     => 'HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings',
		value   => 'ProxyServer',
		data    => '127.0.0.1:3128',
		type    => 'string',
	}

	registry::value { 'noproxy-hosts':
		key     => 'HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings',
		value   => 'ProxyOverride',
		data    => 'wasau801mel0004.globaltest.anz.com;csptraining.service.dev;dbdtw01l.unix.anz;dbdtw02l.unix.anz;dccrowd.service.dev;dcjira.service.dev;dcartifactory.service.dev;dcbamboo.service.dev;dcstash.service.dev;dcconfluence.service.dev;mydesk.dev.anz;mypasswordreset.apps.anz;sqlau801mel0077.globaltest.anz.com;cspqa.service.dev;cspdev01.service.dev;sqlau801mel0092.globaltest.anz.com;wasau301mel0088.globaltest.anz.com;sharepoint.apps.anz;fiatintegration.service.dev;csptst.service.dev;sqlau801mel0080.globaltest.anz.com;confluence.service.anz;wasau601mel0003.globaltest.anz.com;wasau601mel0002.globaltest.anz.com;cspsit.service.dev;sqlau801mel0093.globaltest.anz.com;sqlau801mel0075.globaltest.anz.com;sqlau801mel0076.globaltest.anz.com;sqlau801mel0036.globaltest.anz.com;confluence.service.anz;cspdev.service.dev;serveradmin.apps.anz;.corp.anz.com;localhost;wasau301mel0019.globaltest.anz.com;<local>',
		type    => 'string',
	}

	registry::value { 'auto-proxy-config':
		key   => 'HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings',
		value => 'AutoDetect',
		data  => 0,
		type  => 'dword',
	}
}
