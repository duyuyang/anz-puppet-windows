# bootstrap.ps1
# Prepares an ANZ machine to be provisioned by Puppet.
$ErrorActionPreference = 'stop'
$source = "\\sqlau801mel0075.globaltest.anz.com\data\provisioning\files"

$repo = "https://bitbucket.org/duyuyang/anz-puppet-windows.git"
$provisioning = "C:\ProgramData\provisioning.git"
$modules = "{0}\modules" -f $provisioning
$cntlm = "{0}\cntlm-0.92.3-setup.exe" -f $source
$cntlmIniSource = "{0}\cntlm.ini" -f $source
$gitSource = "{0}\Git-1.9.4-preview20140929.exe" -f $source
$git = "C:\Program Files (x86)\Git\bin\git.exe"
$puppetSource = "{0}\puppet-3.7.3-x64.msi" -f $source
$puppet = "C:\Program Files\Puppet Labs\Puppet\bin\puppet.bat"
$osname = Get-WmiObject Win32_OperatingSystem | select -expand Caption
$buildtype = $null

If($osname -like "*Windows Server*") {
    $buildtype = 'buildagent'
} Else {
    $buildtype = 'workstation'
}

Write-Output "Bootstrapping a $buildtype..."

If($buildtype -eq $null) {
    Write-Error "Could not figure what kind of a system to bootstrap!"
}

Function Validate {
    If(!(Test-Path $source)) { Write-Error "Could not find source $source" }
    If(!(Test-Path $cntlm)) { Write-Error "Could not find CNTLM installer $cntlm" }
    If(!(Test-Path $gitSource)) { Write-Error "Could not find Git installer $git" }
    If(!(Test-Path $puppetSource)) { Write-Error "Could not find the Puppet installer at $puppetSource" }
    Write-Output "Validation Succeeded. Commencing Bootstrap"
}

Function Install-CNTLM {
    $iniPath = "C:\Program Files (x86)\Cntlm\cntlm.ini"
    If(!(Test-Path $iniPath)) {
        Write-Output "Installing CNTLM"
        & $cntlm /silent | Out-Null
    } Else {
        Write-Output "CNTLM is already installed"
    }
    
    Copy-Item $cntlmIniSource $iniPath -Force
    Restart-Service cntlm
}

Function Install-Git {
    If(!(Test-Path $git)) {
        Write-Output "Installing Git"
        & $gitSource /VerySilent | Out-Null
	$gitBinDir = 'C:\Program Files (x86)\Git\bin'
	$currentPath = (Get-ItemProperty -Path 'Registry::HKLM\System\CurrentControlSet\Control\Session Manager\Environment' -name PATH).path
	If(!($env:path | select-string -simpleMatch $gitBinDir)) {
		Set-ItemProperty -Path 'Registry::HKLM\System\CurrentControlSet\Control\Session Manager\Environment' -name PATH -Value "$currentPath;$gitBinDir"
		$env:path = "$currentPath;$gitBinDir"
	}	
    } else {
        Write-Output "Git is already installed"
    }
}

Function Install-Puppet {
    If(!(Test-Path $puppet)) {
        Write-Output "Installing Puppet"
        & msiexec.exe /i $puppetSource /qn /norestart | Out-Null
    } Else {
        Write-Output "Puppet already installed"
    }
}

Function Clone-Repo {
    If(!(Test-Path $provisioning)) {
        $env:https_proxy = "http://127.0.0.1:3128"
        # Need to disable this globably to clone the repo
        & $git config --global http.sslVerify false
        Write-Output "Getting repo $repo"
        & $git clone $repo $provisioning
        cd $provisioning
        # Disable this inside the repo - neccessary for the scheduled task running as SYSTEM to read the setting
        & $git config http.sslVerify false
    }
}

Function Invoke-Puppet {
    Write-Output "Bootstrap complete, triggering Puppet"
    # Start a separate process so it inherits the updated path.
    $env:http_proxy = '127.0.0.1:3128'
    $env:https_proxy = '127.0.0.1:3128'
    & $puppet apply "$($modules)\roles\manifests\$($buildtype).pp" --modulepath $modules -vd
}

Validate
Install-CNTLM
Install-Git
Install-Puppet
Clone-Repo
Invoke-Puppet
