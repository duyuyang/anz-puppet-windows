$errorActionPreference = "STOP"

$proxypac = "http://gblproxypac.lb.service.anz:9001/gblproxy.pac"
$proxy = "http://gblproxy.lb.service.anz" # Used by Gem.exe later on
$source = "\\sqlau801mel0075.globaltest.anz.com\Data"
$casrc = "$source\root_ca.cer"


Write-Output "Setting Proxy"
& reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings" /v AutoConfigURL /t REG_SZ /d $proxypac /f | Out-Null


# Add the root CA for ANZ GLOBAL. Necessary to allow SSL communication.
Write-Output "Configuring this computer to trust the ANZ GLOBAL Root CA"
& certutil.exe -addstore "Root" $casrc | Out-Null

# Install chocolatey
If(-not (Test-Path "C:\ProgramData\Chocolatey")) {
    $wc=new-object net.webclient
    $wp=[system.net.WebProxy]::GetDefaultProxy()
    $wp.UseDefaultCredentials=$true
    $wc.Proxy=$wp
    iex ($wc.DownloadString('https://chocolatey.org/install.ps1')) | Out-Null
}


Write-Output "Installing Java 6 x64 JDK"
$java6src = "$source\jdk-6u45-windows-x64.exe"
& $java6src /s

$choco = "C:\ProgramData\chocolatey\bin\cinst.exe"

& $choco cntlm
& $choco DotNet4.5.1
& $choco git
& $choco javaruntime
& $choco gradle
& $choco SourceTree
& $choco Console2
& $choco intellijidea-ultimate
& $choco vim
& $choco ruby
& $choco ruby2.DevKit
& $choco nodejs.install


If((Test-Path "C:\Program Files\Microsoft SQL Server\110\LocalDB") -and (Test-Path "C:\Program Files (x86)\Microsoft SQL Server\110\Tools\Binn\ManagementStudio\Ssms.exe")) {
    Write-Output "SQL Management Studio and LocalDB are already installed, good"
} Else {
    Write-Output "Installing SQL LocalDB and management tools"
    $sqlsetup = "$source\SQLEXPRWT_x64_ENU.exe"
    & $sqlsetup /QUIETSIMPLE=true /Action=INSTALL /IACCEPTSQLSERVERLICENSETERMS /FEATURES=SSMS,LocalDB /UPDATEENABLED=false /ERRORREPORTING=false /SQMREPORTING=0 /SKIPRULES="HasSecurityBackupAndDebugPrivilegesCheck" | Out-Null
    $localDbVer = & SqlLocalDb info
    Write-Output "Installed LocalDB $localDbVer"
}

Write-Output "Installing gems"
$gem = "C:\tools\ruby213\bin\gem.bat"
# Gem doesn't use the Windows certificate store, so it doesn't have the global root CA.
# This means we have to bypass SSL and use HTTP to download gems instead.
# Ironic that the attempts to make things more secure have resulted in less.
Try {
    & $gem install bundler -v '1.7.4' -p $proxy --source "http://rubygems.org"
} Catch {
    $e = $_
    If(Test-Path C:\tools\ruby213\lib\ruby\gems\2.1.0\gems\bundler-1.7.4) {
        Write-Output "Installed bundler"
    } Else {
        $e
    }
}
Write-Output "Installed all the packages. Here's the stuff you need to do manually:"
Write-Output " - Configure proxy for bundle.exe and gem.exe"
Write-Output " - Configure SourceTree with your name and email"
Write-Output " - Configure cntlm"